local util = require('openmw.util')

return {
    textNormalSize = 16,
    sandColor = util.color.rgb(202 / 255, 165 / 255, 96 / 255),
    borderSize = 4,
    padding = 2,
}